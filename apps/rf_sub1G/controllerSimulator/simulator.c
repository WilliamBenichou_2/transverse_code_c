#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "lib/stdio.h"
#include "drivers/serial.h"
#include "drivers/gpio.h"
#include "drivers/ssp.h"
#include "drivers/i2c.h"
#include "drivers/adc.h"
#include "extdrv/cc1101.h"
#include "extdrv/status_led.h"
#include "extdrv/tmp101_temp_sensor.h"
#include "lib/string.h"


#define MODULE_VERSION	0x02
#define MODULE_NAME "RF Sub1G - USB"


#define RF_868MHz  1
#define RF_915MHz  0
#if ((RF_868MHz) + (RF_915MHz) != 1)
#error Either RF_868MHz or RF_915MHz MUST be defined.
#endif


#define DEBUG 1

#define SELECTED_FREQ  FREQ_SEL_48MHz

/***************************************************************************** */
/* Pins configuration */
/* pins blocks are passed to set_pins() for pins configuration.
 * Unused pin blocks can be removed safely with the corresponding set_pins() call
 * All pins blocks may be safelly merged in a single block for single set_pins() call..
 */

#define BUFFER_SIZE 60
#define MSG_SIZE 32
#define RF_BUFFER_SIZE MSG_SIZE + 4

#define DEVICE_ADDRESS  0x01 /* Addresses 0x00 and 0xFF are broadcast */
#define NEIGHBOR_ADDRESS 0x57 /* Address of the associated device */

char sendBuffer[BUFFER_SIZE][MSG_SIZE];
int send_ptr = 0, receive_ptr = 0;

const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* I2C 0 */
	{ LPC_I2C0_SCL_PIO_0_10, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	{ LPC_I2C0_SDA_PIO_0_11, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	/* SPI */
	{ LPC_SSP0_SCLK_PIO_0_14, LPC_IO_DIGITAL },
	{ LPC_SSP0_MOSI_PIO_0_17, LPC_IO_DIGITAL },
	{ LPC_SSP0_MISO_PIO_0_16, LPC_IO_DIGITAL },
	/* ADC */
	{ LPC_ADC_AD0_PIO_0_30, LPC_IO_ANALOG },
	{ LPC_ADC_AD1_PIO_0_31, LPC_IO_ANALOG },
	{ LPC_ADC_AD2_PIO_1_0,  LPC_IO_ANALOG },
	ARRAY_LAST_PIO,
};

const struct pio cc1101_cs_pin = LPC_GPIO_0_15;
const struct pio cc1101_miso_pin = LPC_SSP0_MISO_PIO_0_16;
const struct pio cc1101_gdo0 = LPC_GPIO_0_6;
const struct pio cc1101_gdo2 = LPC_GPIO_0_7;

#define TMP101_ADDR  0x94  /* Pin Addr0 (pin5 of tmp101) connected to VCC */
struct tmp101_sensor_config tmp101_sensor = {
	.bus_num = I2C0,
	.addr = TMP101_ADDR,
	.resolution = TMP_RES_ELEVEN_BITS,
};
const struct pio temp_alert = LPC_GPIO_0_3;

const struct pio status_led_green = LPC_GPIO_0_28;
const struct pio status_led_red = LPC_GPIO_0_29;


#define ADC_VBAT  LPC_ADC(0)
#define ADC_EXT1  LPC_ADC(1)
#define ADC_EXT2  LPC_ADC(2)

/***************************************************************************** */
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	gpio_on();
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}

unsigned char checksum(unsigned char *ptr, size_t sz) {
	unsigned char chk = 0;
	while (sz-- != 0)
		chk -= *ptr++;
	return chk;
}

int isMsgReady() {

#ifdef DEBUG
	//uprintf(UART0, "checking at id %d : ", send_ptr);
	//uprintf(UART0, sendBuffer[send_ptr]);
	//uprintf(UART0, "\n");
#endif
	return strncmp(sendBuffer[send_ptr], "", MSG_SIZE) != 0;
}


/******************************************************************************/
/* RF Communication */

static volatile int check_rx = 0;
void rf_rx_calback(uint32_t gpio)
{
	check_rx = 1;
}

static uint8_t rf_specific_settings[] = {
	CC1101_REGS(gdo_config[2]), 0x07, /* GDO_0 - Assert on CRC OK | Disable temp sensor */
	CC1101_REGS(gdo_config[0]), 0x2E, /* GDO_2 - FIXME : do something usefull with it for tests */
	CC1101_REGS(pkt_ctrl[0]), 0x0F, /* Accept all sync, CRC err auto flush, Append, Addr check and Bcast */
#if (RF_915MHz == 1)
	/* FIXME : Add here a define protected list of settings for 915MHz configuration */
#endif
};

/* RF config */
void rf_config(void)
{
	config_gpio(&cc1101_gdo0, LPC_IO_MODE_PULL_UP, GPIO_DIR_IN, 0);
	cc1101_init(0, &cc1101_cs_pin, &cc1101_miso_pin); /* ssp_num, cs_pin, miso_pin */
													  /* Set default config */
	cc1101_config();
	/* And change application specific settings */
	cc1101_update_config(rf_specific_settings, sizeof(rf_specific_settings));
	set_gpio_callback(rf_rx_calback, &cc1101_gdo0, EDGE_RISING);
	cc1101_set_address(DEVICE_ADDRESS);
#ifdef DEBUG
	uprintf(UART0, "CC1101 RF link init done.\n\r");
#endif
}

void handle_rf_rx_data(void)
{
	//uint8_t data[RF_BUFFER_SIZE];
	//int8_t ret = 0;
	//uint8_t status = 0;

	///* Check for received packet (and get it if any) */
	//ret = cc1101_receive_packet(data, RF_BUFFER_SIZE, &status);

	///* Go back to RX mode */
	//cc1101_enter_rx_mode();
	//char receivedMsg[MSG_SIZE];
	//if (ret == RF_BUFFER_SIZE) {
	//	memcpy(receivedMsg, &data[4], MSG_SIZE);
	//	
	//		uprintf(UART0, "msg %d with id %d confirmed!\n", send_ptr, data[2]);
	//		if (strcmp("ACQ", receivedMsg) == 0 && data[2] == send_ptr) {
	//			strcpy(sendBuffer[send_ptr], "\0\0\0\0\0\0\0\0\0");
	//			send_ptr = (send_ptr + 1) % BUFFER_SIZE;
	//		}
	//	
	//	
	//}


}




static uint32_t cc_tx = 0;
static uint8_t cc_tx_buff[MSG_SIZE];
static uint8_t cc_ptr = 0;

void storeInSendBuffer() {
	strncpy(sendBuffer[receive_ptr], cc_tx_buff, MSG_SIZE);
	receive_ptr = (receive_ptr + 1) % BUFFER_SIZE;
}

void handle_uart_cmd(uint8_t c)
{
	status_led(red_on);
	if ((c != '\n') && (c != '\r')) {
		if (cc_ptr < MSG_SIZE) {
			cc_tx_buff[cc_ptr] = c;
			cc_ptr++;
		}
		else {
			cc_ptr = 0;
		}
	}
	else{
		cc_tx = 1;
		if (cc_ptr > 0 && cc_ptr < MSG_SIZE) {
			cc_tx_buff[cc_ptr] = '\0';
			storeInSendBuffer();
			cc_ptr = 0;
		}
		
	}
}

void send_on_rf(void)
{
	status_led(green_on);
	uint8_t cc_tx_data[RF_BUFFER_SIZE];
	uint8_t tx_len = cc_ptr;

	int ret = 0;


	cc_ptr = 0;
	/* Prepare buffer for sending */
	cc_tx_data[0] = RF_BUFFER_SIZE - 1;
	cc_tx_data[1] = NEIGHBOR_ADDRESS;
	cc_tx_data[2] = send_ptr;
	cc_tx_data[3] = checksum(sendBuffer[send_ptr], MSG_SIZE);
	memcpy(&cc_tx_data[4], sendBuffer[send_ptr], MSG_SIZE);
	/* Send */
	if (cc1101_tx_fifo_state() != 0) {
		cc1101_flush_tx_fifo();
	}

	//check if sendPtr wasn't modified by an interruption
	if (isMsgReady()) {
		ret = cc1101_send_packet(cc_tx_data, (RF_BUFFER_SIZE));

#ifdef DEBUG
		uprintf(UART0, "sending %d characters with send_ptr %d\n", strlen(sendBuffer[send_ptr]), send_ptr);
		uprintf(UART0, &cc_tx_data[3]);
		uprintf(UART0, "\n");
#endif

		
	}
	

	strcpy(sendBuffer[send_ptr], "\0\0\0\0\0\0\0\0\0");
	send_ptr = (send_ptr + 1) % BUFFER_SIZE;

	//Moved on confimation received
	//send_ptr = (send_ptr + 1) % BUFFER_SIZE;
}


void init_send_buffer() {
	for (size_t i = 0; i < BUFFER_SIZE; i++)
	{
		strncpy(sendBuffer[i], "", MSG_SIZE);
	}
}

/***************************************************************************** */
int main(void)
{
	system_init();
	init_send_buffer();

	uart_on(UART0, 9600, handle_uart_cmd);
	ssp_master_on(0, LPC_SSP_FRAME_SPI, 8, 4 * 1000 * 1000); /* bus_num, frame_type, data_width, rate */
	adc_on(NULL);
	status_led_config(&status_led_green, &status_led_red);

	/* Radio */
	rf_config();


	while (1) {
		uint8_t status = 0;

		//If there is data to send
		if (isMsgReady()) {
			send_on_rf();
			msleep(500);
		}
		else {
			status_led(green_off);
			msleep(250);
			status_led(green_on);
			msleep(250);
		}


		/* RF */
		if (cc_tx == 1) {
			status_led(red_off);
			cc_tx = 0;
		}

		/* Do not leave radio in an unknown or unwated state */
		do {
			status = (cc1101_read_status() & CC1101_STATE_MASK);
		} while (status == CC1101_STATE_TX);

		if (status != CC1101_STATE_RX) {
			static uint8_t loop = 0;
			loop++;
			if (loop > 10) {
				if (cc1101_rx_fifo_state() != 0) {
					cc1101_flush_rx_fifo();
				}
				cc1101_enter_rx_mode();
				loop = 0;
			}
		}

		if (check_rx == 1) {

			check_rx = 0;
			handle_rf_rx_data();
		}

	}
	return 0;
}





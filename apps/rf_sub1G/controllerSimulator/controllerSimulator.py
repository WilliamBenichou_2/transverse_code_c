from tkinter import *
import serial
import sys

# Graphic interface for the send program
master = Tk()
scales=list()
NB_SCALES=60

for i in range(NB_SCALES):
    widget=Scale(master, from_=9, to=0) # creates widgetidget
    widget.grid(row=i//10,column=i-(i//10)*10)
    scales.append(widget) # stores widgetidget in scales list

# send serial message 
# Don't forget to establish the right serial port ******** ATTENTION
# SERIALPORT_NAME = "/dev/ttyUSB0"
SERIALPORT_NAME = "/dev/ttyS5"
BAUDRATE = 12000
serialPort = serial.Serial()
def initUART():

    if serialButton['text'] == "Open Serial":   
        # serialPort = serial.Serial(SERIALPORT_NAME, BAUDRATE)
        serialPort.port=SERIALPORT_NAME
        serialPort.baudrate=BAUDRATE
        serialPort.bytesize = serial.EIGHTBITS #number of bits per bytes
        serialPort.parity = serial.PARITY_NONE #set parity check: no parity
        serialPort.stopbits = serial.STOPBITS_ONE #number of stop bits
        serialPort.timeout = None          #block rea
        # serialPort.timeout = 0             #non-block read
        # serialPort.timeout = 2              #timeout block read
        serialPort.xonxoff = False     #disable softwidgetare flowidget control
        serialPort.rtscts = False     #disable hardwidgetare (RTS/CTS) flowidget control
        serialPort.dsrdtr = False       #disable hardwidgetare (DSR/DTR) flowidget control
        #serialPort.writeTimeout = 0     #timeout for write
        print ("Starting Up Serial Monitor")
        try:
                serialPort.open()
        except serial.SerialException:
                print("Serial {} port not available".format(SERIALPORT_NAME))
                exit()
        serialButton['text'] = "Close Serial"
        btn['state'] = 'normal'
    else:
        serialPort.close()
        serialButton['text'] = "Open Serial"
        btn['state'] = 'disabled'


def sendUARTMessage(msg):
    serialPort.write(msg.encode())
    print("Message <" + msg + "> sent to micro-controller." )


def read_scales():
    btn['state'] = 'disabled'
    for i in range(NB_SCALES):
        column = i-(i//10)*10
        row = i//10
        if (scales[i].get()>0) :
                print("Fire x=%d, y=%d has value %d" %( row, column, scales[i].get()) )
        sendUARTMessage("{%d;%d;%d}" %(row, column, scales[i].get()))
    
    btn['state'] = 'normal'


if len(sys.argv) > 1:
    SERIALPORT_NAME = "/dev/" + sys.argv[1]
btn=Button(master,text="Send Values",highlightcolor="blue",command=read_scales, state="disabled") # button to read values
serialButton=Button(master,text="Open Serial",highlightcolor="blue",command=initUART) # button to read values
btn.grid(row=6,column=7,columnspan = 3)
serialButton.grid(row=6, column=0, columnspan = 3)

# initUART()

mainloop()